from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from qa import views


urlpatterns = [
    path('questions', views.QuestionList.as_view()),
    path('questions/<int:pk>', views.QuestionDetail.as_view()),
    path('questions/<int:pk>/answers', views.AnswerList.as_view()),
    path('questions/<int:pk>', views.AnswerList.as_view(), 
    	name='answer-detail'),
    path('users/', views.UserList.as_view()),
    path('users/<int:pk>/', views.UserDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
