from django.contrib.auth.models import User
from rest_framework import serializers
from qa.models import Question, Answer

class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ('user', 'question', 'content', 'votes')


class QuestionSerializer(serializers.ModelSerializer):
    answers = AnswerSerializer(many=True, read_only=True)

    class Meta:
        model = Question
        fields = ('id', 'title', 'description', 'answers')

    def create(self, validated_data):
        return Question.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get(
            'description', instance.description)
        instance.tags = validated_data.get('tags', instance.tags)
        instance.save()
        return instance


class UserSerializer(serializers.ModelSerializer):
    questions = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Question.objects.all())

    class Meta:
        model = User
        fields = ('id', 'username', 'questions')


class AnswerListSerializer(serializers.ModelSerializer):
    question = serializers.HyperlinkedRelatedField(
        lookup_field = 'pk',
        read_only=True,
        view_name='answer-detail'
    )
    class Meta(object):
       model = Answer
       fields = ('user', 'question', 'content', 'votes')
    